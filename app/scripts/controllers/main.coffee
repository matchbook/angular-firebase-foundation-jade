"use strict"

angular.module("DevApp").controller "MainCtrl", ($scope, angularFire) ->
	url = "https://holon.firebaseio.com/devapp"
	loadFirebase = angularFire(url, $scope, 'awesomeThings')
	$scope.awesomeThings = ["HTML5 Boilerplate", "AngularJS", "Karma"]

	loadFirebase.then ->
  	$scope.awesomeThings.push("Another one")

  	$scope.addThing = (thing) ->
  		$scope.awesomeThings.push(thing)

  	$scope.removeThing = (idx) ->
  		$scope.awesomeThings.splice(idx, 1)


